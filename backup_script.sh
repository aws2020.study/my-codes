#!/bin/bash


#Check if log directory exists or else create one to track droplet logs

if [ -d "/var/log/doctl-remote-snapshot" ]
        then
                :
        else
                sudo mkdir /var/log/doctl-remote-snapshot
fi
logdate=$(date +%Y-%m-%d@%H:%M)
exec > >(tee -i /var/log/doctl-remote-snapshot/"$logdate".log)

today=$(date '+%A, %B %d, %Y at %I:%M%p %Z')

#Check positional argument to create backup

#Create cloudmyplacedb snapshot
db_snap() {
  name='app.cloudmyplace-db'
  echo "Creating snapshot titled \"$name$today\""
  echo "Please wait this may take awhile. About 1 minute per GB."
  sudo /snap/bin/doctl compute droplet-action snapshot --snapshot-name "$name$today" 184196196 --wait
  echo
  echo "Creation of  snapshot titled \"$name$today\" successful"
  echo
}
#Create mybot-test snapshot
mybot-test() {
  name='mybot-test'
  echo "Creating snapshot titled \"$name$today\""
  echo "Please wait this may take awhile. About 1 minute per GB."
  sudo /snap/bin/doctl compute droplet-action snapshot --snapshot-name "$name$today" 186688297 --wait
echo
}

mybot-staging() {
  name='mybot2.0'
  echo "Creating snapshot titled \"$name$today\""
  echo "Please wait this may take awhile. About 1 minute per GB."
  sudo /snap/bin/doctl compute droplet-action snapshot --snapshot-name "$name$today" 190689117 --wait
  echo
  echo "Creation of  snapshot titled \"$name$today\" successful"
  echo
}

mybot-prod() {
  name='mybot-prod'
  echo "Creating snapshot titled \"$name$today\""
  echo "Please wait this may take awhile. About 1 minute per GB."
  sudo /snap/bin/doctl compute droplet-action snapshot --snapshot-name "$name$today" 135439715 --wait
  echo
  echo "Creation of  snapshot titled \"$name$today\" successful"
  echo
}

cloudmyplace-testing() {
  name='cloudmyplace-testing'
  echo "Creating snapshot titled \"$name$today\""
  echo "Please wait this may take awhile. About 1 minute per GB."
  sudo /snap/bin/doctl compute droplet-action snapshot --snapshot-name "$name$today" 186712555 --wait
  echo
  echo "Creation of  snapshot titled \"$name$today\" successful"
  echo
}

cloudmyplace-prod() {
  name='apps-cloudmyplace'
    echo "Creating snapshot titled \"$name$today\""
  echo "Please wait this may take awhile. About 1 minute per GB."
  sudo /snap/bin/doctl compute droplet-action snapshot --snapshot-name "$name$today" 159338239 --wait
  echo
  echo "Creation of  snapshot titled \"$name$today\" successful"
  echo
}

success_msg() {
  echo "Snapshots of servers are successfully created"
  echo "Available images are:"
  echo
  doctl compute snapshot list --format "Name, Size, Regions"
  exit 0
}

db_oldsnap_del() {
  #Set the variable
  dropletid=184196196
  dropletname='app.cloudmyplace-db'
  #List the no of snapshots and count them
  snapshots_num=$(sudo /snap/bin/doctl compute image list-user --format "ID,Type,Name" --no-header | grep snapshot | grep $dropletname$

  #Mention how many number of snapshots to retain. For DB -3 and for app server -2
  numretain=3

  diff_no=$(($snapshots_num - $numretain))
  echo "Deleting the last "$diff_no" snapshot(s)"


  oldest=$(doctl compute snapshot  list --resource droplet --format "ID,Name,CreatedAt,ResourceId" | grep $dropletid | sort -rk 3 | cu$


  b='true'
  while $b; do
                if [[ "$snapshots_num" -gt "$numretain" ]]
                then
                        oldest=$(doctl compute snapshot  list --resource droplet --format "ID,Name,CreatedAt,ResourceId" | grep $dropl$
                        sudo /snap/bin/doctl compute image delete $oldest --force
                fi
                b='false'
  done
   echo "Available snapshots of $dropletname are:"
   sudo /snap/bin/doctl compute image list-user --format "ID,Type,Name" | grep snapshot | grep $dropletname
}
success_msg
help() {
  echo "The servers available are to take snapshot"
  echo
  doctl compute droplet list --format "ID, Name"
  echo "Type the below command to take snapshots"
  echo "backup.sh <any of the below command>"
  echo "dbbackup"
  echo "mybot-test"
case "${1}" in
  dbbackup)
    db_snap
    ;;
  mybottest)
    mybot-test
    ;;
  mybotstaging)
    mybot-staging
    ;;
  mybotprod)
    mybot-prod
    ;;
  cloudmyplacetest)
    cloudmyplace-testing
    ;;
  cloudmyplaceprod)
    cloudmyplace-prod
    ;;
  help)
    help
    ;;
esac

success-msg
